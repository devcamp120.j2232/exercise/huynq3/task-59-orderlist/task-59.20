package com.devcamp.task5920jpaorder.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5920jpaorder.model.COrder;
import com.devcamp.task5920jpaorder.repository.IOrderRepository;

@RestController
@CrossOrigin
public class COrderController {
    @Autowired
    IOrderRepository orderRepository;
    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders(){
        try {
            List<COrder> orderList = new ArrayList<>();
            orderRepository.findAll().forEach(orderList::add);
            if (orderList.size() ==0){
                return new ResponseEntity<>(orderList, HttpStatus.NOT_FOUND);
            }
            else return new ResponseEntity<>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
