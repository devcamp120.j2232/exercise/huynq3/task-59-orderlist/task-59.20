package com.devcamp.task5920jpaorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5920JpaOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5920JpaOrderApplication.class, args);
	}

}
